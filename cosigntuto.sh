$ cosign generate-key-pair

$ cosign sign \
<namespace>/<image>:<version>

$ syft <namespace>/<image>:<version> \
--scope all-layers -o spdx-json > output.json

$ syft <namespace>/<image>:<version> \
--scope all-layers -o cyclonedx-json > SBOM-output.json

$ cosign verify --key <key> <namespace>/<image>:<version> | jq . 

$ cosign verify \
    --certificate-identity username@gmail.com \
    --certificate-oidc-issuer https://accounts.google.com \
    docker-username/hello-container

$ cosign attest --predicate <file> \
--key cosign.key $IMAGE_URI_DIGEST

$ cosign verify-attestation \
--key cosign.pub $IMAGE_URI | jq --slurp \
'map(.payload | @base64d | fromjson \
| .predicate.Data | fromjson )'

#### HARBOR DEMO ####
$ docker build . --tag repo.harbor/cosign/alpha:1.2.0

$ docker push repo.harbor/cosign/alpha:1.2.0

$ cosign sign --key ~/cosign.key repo.harbor/cosign/alpha:1.2.0

$ syft repo.harbor/cosign/alpha:1.2.0 \
--scope all-layers -o spdx-json > alpha.json

$ cosign attest --predicate alpha.json \
--key ~/cosign.key repo.harbor/cosign/alpha:1.2.0

$ cosign verify-attestation --key ~/cosign.pub \ 
  repo.harbor/cosign/alpha:1.2.0 | jq --slurp \
  'map(.payload | @base64d | fromjson | .predicate.Data | fromjson )' 

$ cosign verify --key ~/cosign.pub repo.harbor/cosign/alpha:1.2.0 | jq .

$ syft repo.harbor/cosign/alpha:1.2.0 \
--scope all-layers -o cyclonedx-json > alphaSBOM.json




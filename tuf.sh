docker run -d -p 5001:5000 -e REGISTRY_STORAGE_DELETE_ENABLED=true --name registry registry

docker build . -t localhost:5001/demo:1.0.0

docker push localhost:5001/demo:1.0.0

syft localhost:5001/demo:1.0.0 --scope all-layers -o spdx-json | jq . > provenance.json

oras attach --artifact-type sbom/example localhost:5001/demo:1.0.0 provenance.json:application/json

IMAGE=localhost:5001/demo@

PROVENANCE=localhost:5001/demo@

IMAGE=localhost:5001/demo@sha256:9e83bf93824ac55dab94d47a79751252b3db3d04e02636c7899c4fc1bea297a1

notation cert generate-test --default "registry.root"

notation key ls

notation cert ls

notation sign $IMAGE

notation sign $PROVENANCE

notation sign --signature-format cose $IMAGE

notation ls $IMAGE

cat <<EOF > ./trustpolicy.json
{
    "version": "1.0",
    "trustPolicies": [
        {
            "name": "demo-images",
            "registryScopes": [ "*" ],
            "signatureVerification": {
                "level" : "strict" 
            },
            "trustStores": [ "ca:registry.root" ],
            "trustedIdentities": [
                "*"
            ]
        }
    ]
}
EOF

notation policy import ./trustpolicy.json

notation policy show

notation verify $IMAGE

docker rm -f registry

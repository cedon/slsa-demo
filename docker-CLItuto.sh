docker buildx build --tag <namespace>/<image>:<version> \
    --attest type=provenance,mode=[min,max] .

docker buildx imagetools inspect <namespace>/<image>:<version> \
    --format "{{ json .Provenance.SLSA }}"

#### DOCKER BUILDX DEMO ######
export DOCKER_BUILDKIT=0
export COMPOSE_DOCKER_CLI_BUILD=0

docker buildx create \
  --name container \
  --driver=docker-container

docker buildx build --tag repo.harbor/demo/demo-z:1.0.0 \
    --builder=container \
    --attest type=provenance,mode=max . --output=output-path

docker buildx imagetools inspect repo.harbor/cosign/nginx:2.0.0 \
    --builder=container \
    --format "{{ json .Provenance }}"

docker buildx imagetools inspect repo.harbor/demo/demo-z:1.0.0 \
    --builder=container \
    --format "{{ json .Provenance.SLSA }}"

docker buildx imagetools inspect cedon/alpine:1.0.0 \
    --builder=container \
    --format "{{ json .Provenance.SLSA }}"
